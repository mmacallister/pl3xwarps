package net.pl3x.bukkit.warps;

import net.pl3x.bukkit.warps.command.DelWarp;
import net.pl3x.bukkit.warps.command.ListWarps;
import net.pl3x.bukkit.warps.command.SetWarp;
import net.pl3x.bukkit.warps.command.Warp;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import java.io.IOException;

public class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        getCommand("delwarp").setExecutor(new DelWarp());
        getCommand("listwarps").setExecutor(new ListWarps());
        getCommand("setwarp").setExecutor(new SetWarp());
        getCommand("warp").setExecutor(new Warp());

        try {
            Metrics metrics = new Metrics(this);
            metrics.start();
        } catch (IOException e) {
            Logger.info("&4Failed to start Metrics: &e" + e.getMessage());
        }

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }
}
