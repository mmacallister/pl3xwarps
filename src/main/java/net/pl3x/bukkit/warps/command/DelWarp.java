package net.pl3x.bukkit.warps.command;

import net.pl3x.bukkit.warps.configuration.Lang;
import net.pl3x.bukkit.warps.configuration.WarpConfig;
import net.pl3x.bukkit.warps.manager.ChatManager;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class DelWarp implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            return WarpConfig.getConfig().getMatchingWarps(args[0].trim());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            ChatManager.sendMessage(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.delwarp")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length < 1) {
            ChatManager.sendMessage(sender, Lang.MUST_SPECIFY_WARP_NAME);
            return true;
        }

        WarpConfig wConfig = WarpConfig.getConfig();
        String warpName = args[0].trim().toLowerCase();
        Location warp = wConfig.getWarp(warpName);

        if (warp == null) {
            ChatManager.sendMessage(sender, Lang.WARP_NOT_FOUND.replace("{warp}", warpName));
            return true;
        }

        wConfig.deleteWarp(warpName);
        ChatManager.sendMessage(sender, Lang.WARP_DELETED.replace("{warp}", warpName));
        return true;
    }
}
