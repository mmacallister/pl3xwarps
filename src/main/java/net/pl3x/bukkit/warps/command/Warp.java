package net.pl3x.bukkit.warps.command;

import net.pl3x.bukkit.warps.configuration.Config;
import net.pl3x.bukkit.warps.configuration.Lang;
import net.pl3x.bukkit.warps.configuration.WarpConfig;
import net.pl3x.bukkit.warps.manager.ChatManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class Warp implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            return WarpConfig.getConfig().getMatchingWarps(args[0].trim());
        }
        if (args.length == 2) {
            String name = args[1].trim().toLowerCase();
            List<String> list = new ArrayList<>();
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getName().toLowerCase().startsWith(name)) {
                    list.add(player.getName());
                }
            }
            return list;
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            ChatManager.sendMessage(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.warp")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length < 1) {
            ChatManager.sendMessage(sender, Lang.MUST_SPECIFY_WARP_NAME);
            return true;
        }

        Player player = (Player) sender;
        String warpName = args[0].trim().toLowerCase();
        Location warp = WarpConfig.getConfig().getWarp(warpName);

        if (warp == null) {
            ChatManager.sendMessage(sender, Lang.WARP_NOT_FOUND.replace("{warp}", warpName));
            return true;
        }

        if (Config.USE_WARP_PERMS.getBoolean() && !sender.hasPermission("command.warp." + warpName)) {
            ChatManager.sendMessage(sender, Lang.NO_WARP_PERM);
            return true;
        }

        if (args.length > 1) {
            if (!sender.hasPermission("others.warp")) {
                ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }

            Player target = Bukkit.getPlayer(args[1]);
            if (target == null) {
                ChatManager.sendMessage(sender, Lang.PLAYER_NOT_FOUND);
                return true;
            }

            if (target.hasPermission("exempt.warp")) {
                ChatManager.sendMessage(sender, Lang.WARP_EXEMPT);
                return true;
            }

            target.teleport(warp);
            ChatManager.sendMessage(sender, Lang.WARP_OTHER.replace("{player}", target.getName()).replace("{warp}", warpName));
            ChatManager.sendMessage(target, Lang.WARP_TARGET.replace("player}", player.getName()).replace("{warp}", warpName));
            return true;
        }

        player.teleport(warp);
        ChatManager.sendMessage(sender, Lang.WARP.replace("{warp}", warpName));
        return true;
    }
}
