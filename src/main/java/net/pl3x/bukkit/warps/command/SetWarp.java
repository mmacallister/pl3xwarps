package net.pl3x.bukkit.warps.command;

import net.pl3x.bukkit.warps.configuration.Lang;
import net.pl3x.bukkit.warps.configuration.WarpConfig;
import net.pl3x.bukkit.warps.manager.ChatManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class SetWarp implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            return WarpConfig.getConfig().getMatchingWarps(args[0].trim());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            ChatManager.sendMessage(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.setwarp")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length < 1) {
            ChatManager.sendMessage(sender, Lang.MUST_SPECIFY_WARP_NAME);
            return true;
        }

        String warpName = args[0].trim().toLowerCase();

        WarpConfig.getConfig().setWarp(warpName, ((Player) sender).getLocation());
        ChatManager.sendMessage(sender, Lang.WARP_SET.replace("{warp}", warpName));
        return true;
    }
}
