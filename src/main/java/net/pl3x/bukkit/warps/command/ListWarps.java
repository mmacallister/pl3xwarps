package net.pl3x.bukkit.warps.command;

import net.pl3x.bukkit.warps.configuration.Lang;
import net.pl3x.bukkit.warps.configuration.WarpConfig;
import net.pl3x.bukkit.warps.manager.ChatManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.List;

public class ListWarps implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("command.listwarps")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        List<String> warps = WarpConfig.getConfig().getWarps();

        if (warps == null || warps.isEmpty()) {
            ChatManager.sendMessage(sender, Lang.WARP_LIST_EMPTY);
            return true;
        }

        ChatManager.sendMessage(sender, Lang.WARP_LIST.replace("{list}", warps.toString()));
        return true;
    }
}
