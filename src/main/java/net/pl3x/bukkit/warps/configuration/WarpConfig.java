package net.pl3x.bukkit.warps.configuration;

import net.pl3x.bukkit.warps.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WarpConfig extends YamlConfiguration {
    private static WarpConfig config;

    public static WarpConfig getConfig() {
        if (config == null) {
            config = new WarpConfig();
        }
        return config;
    }

    private final File file;
    private final Object saveLock = new Object();

    public WarpConfig() {
        super();
        file = new File(Main.getPlugin(Main.class).getDataFolder(), "warps.yml");
        load();
    }

    private void load() {
        synchronized (saveLock) {
            try {
                this.load(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }

    public List<String> getWarps() {
        List<String> warps = new ArrayList<>();
        if (!isSet("warps")) {
            return warps;
        }
        for (String warp : getConfigurationSection("warps").getValues(false).keySet()) {
            warps.add(warp.toLowerCase());
        }
        Collections.sort(warps);
        return warps;
    }

    public List<String> getMatchingWarps(String name) {
        name = name.toLowerCase();
        List<String> list = new ArrayList<>();
        for (String warp : getWarps()) {
            if (warp.toLowerCase().startsWith(name)) {
                list.add(warp);
            }
        }
        return list;
    }

    public Location getWarp(String warp) {
        double x = getDouble("warps." + warp + ".x");
        double y = getDouble("warps." + warp + ".y");
        double z = getDouble("warps." + warp + ".z");
        World world = Bukkit.getWorld(getString("warps." + warp + ".world", ""));
        float pitch = (float) getDouble("warps." + warp + ".pitch");
        float yaw = (float) getDouble("warps." + warp + ".yaw");
        if (world == null) {
            return null;
        }
        return new Location(world, x, y, z, yaw, pitch);
    }

    public void setWarp(String warp, Location location) {
        set("warps." + warp + ".x", location.getX());
        set("warps." + warp + ".y", location.getY());
        set("warps." + warp + ".z", location.getZ());
        set("warps." + warp + ".world", location.getWorld().getName());
        set("warps." + warp + ".pitch", location.getPitch());
        set("warps." + warp + ".yaw", location.getYaw());
        save();
    }

    public void deleteWarp(String warp) {
        set("warps." + warp, null);
        save();
    }
}
