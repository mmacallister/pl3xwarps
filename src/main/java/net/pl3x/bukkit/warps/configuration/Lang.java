package net.pl3x.bukkit.warps.configuration;

import net.pl3x.bukkit.warps.Logger;
import net.pl3x.bukkit.warps.Main;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Lang {
    PLAYER_COMMAND("&4This command is only available to players."),
    COMMAND_NO_PERMISSION("&4You do not have permission for this command!"),
    PLAYER_NOT_FOUND("&4Cannot find that player!"),
    WARP_NOT_FOUND("&4Cannot find that warp!"),
    WARP_EXEMPT("&4You cannot warp that player!"),
    MUST_SPECIFY_WARP_NAME("&4Must specify a warp name!"),
    WARP_LIST_EMPTY("&4No warps are set!"),
    NO_WARP_PERM("&4You do not have permission to use that warp!"),
    WARP_SET("&dWarp &7{warp} &dhas been set."),
    WARP_DELETED("&dWarp &7{warp} &dhas been deleted."),
    WARP("&dYou have warped to &7{warp}&d."),
    WARP_OTHER("&dYou have warped &7{player} &dto &7{warp}&d."),
    WARP_TARGET("&dYou have warped to &7{warp} &dby &7{player}&d."),
    WARP_LIST("&dWarps: {list}");

    private Main plugin;
    private String def;

    private File configFile;
    private FileConfiguration config;

    Lang(String def) {
        this.plugin = Main.getPlugin(Main.class);
        this.def = def;
        configFile = new File(plugin.getDataFolder(), Config.LANGUAGE_FILE.getString());
        saveDefault();
        reload();
    }

    public void reload() {
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private void saveDefault() {
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE.getString(), false);
        }
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    @Override
    public String toString() {
        String value = config.getString(getKey(), def);
        if (value == null) {
            Logger.error("Missing lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
